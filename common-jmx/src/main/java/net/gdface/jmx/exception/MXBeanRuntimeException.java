package net.gdface.jmx.exception;

/**
 * 
 * MXBean实例调用异常
 * @author guyadong
 *
 */
public class MXBeanRuntimeException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public MXBeanRuntimeException() {
	}

	public MXBeanRuntimeException(String message) {
		super(message);
	}

	public MXBeanRuntimeException(Throwable cause) {
		super(cause);
	}

	public MXBeanRuntimeException(String message, Throwable cause) {
		super(message, cause);
	}

}
