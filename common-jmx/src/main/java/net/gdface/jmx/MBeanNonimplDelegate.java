package net.gdface.jmx;

import javax.management.JMX;
import javax.management.StandardMBean;

import net.gdface.utils.InterfaceNonimplDecorator;

/**
 * 基于{@link InterfaceNonimplDecorator},实现MBean接口封装<br>
 * 
 * @author guyadong
 *
 * @param <I> 接口类型
 */
public class MBeanNonimplDelegate<I> extends BaseMBeanDelegate<I>{
	private final InterfaceNonimplDecorator<I> mbeanDecroator;

	@SuppressWarnings("unchecked")
	public MBeanNonimplDelegate(Object delegate) {
		super((Class<I>)JMXSupport.getMBeanClass(delegate));
		this.mbeanDecroator = new InterfaceNonimplDecorator<I>(
				mbeanClass, delegate);
	}
	public MBeanNonimplDelegate(Object delegate,Class<I> mbeanClass) {
		super(mbeanClass);
		this.mbeanDecroator = new InterfaceNonimplDecorator<I>(
				mbeanClass, delegate);
	}
	@Override
	protected Object getMBean(){
		return new StandardMBean(mbeanDecroator.proxyInstance(),mbeanClass,JMX.isMXBeanInterface(mbeanClass));
	}
	/**
	 * 返回当前代理实例
	 * @return 代理实例
	 */
	public Object delegate() {
		return mbeanDecroator.delegate();
	}
}


