package net.gdface.utils;

import static com.google.common.base.Preconditions.*;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MulticastSocket;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import com.google.common.base.MoreObjects;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.base.Strings;
import com.google.common.net.HostAndPort;

import net.gdface.utils.NetworkUtil.Filter;

/**
 * 组播/广播数据接收处理器
 * @author guyadong
 *
 */
public class MultiCastDispatcher implements Runnable{
	public static final int DEFAULT_BUFFER_SIZE = 1024;
	private final InetAddress group;
	private final int port;
	private final Predicate<byte[]> processor;
	private final Predicate<Throwable> onerr;
	private Boolean stopListener = null;
	private  final byte[] message;
	private DatagramSocket datagramSocket;
	/**
	 * 构造方法
	 * @param group 组播或广播地址
	 * @param port 端口号
	 * @param bufferSize 组播数据最大长度，根据此参数值分配数据接收缓冲区长度
	 * @param processor 数据处理器,返回false,则中止循环
	 * @param onerr 异常处理器,返回false,则中止循环，为{@code null}则使用默认值{@link Predicates#alwaysTrue}
	 */
	public MultiCastDispatcher(InetAddress group,int port,int bufferSize,
			Predicate<byte[]>processor,
			Predicate<Throwable> onerr){
		checkArgument(null != group,"group is null");
		checkArgument(group.isMulticastAddress() || NetworkUtil.isBroadcast(group),"group %s is not a multicast or broadcast address",group);
		this.group = group;
		this.port = port;
		this.processor = checkNotNull(processor,"processor is null");
		this.onerr = MoreObjects.firstNonNull(onerr, Predicates.<Throwable>alwaysTrue());
		this.message = new byte[bufferSize <= 0 ? DEFAULT_BUFFER_SIZE : bufferSize];
	}
	/**
	 * 构造方法
	 * @param bindaddr 组播或广播IP地址
	 * @param port 端口号
	 * @param bufferSize
	 * @param processor
	 * @param onerr
	 * @throws UnknownHostException
	 * @see #MultiCastDispatcher(InetAddress, int, int, Predicate, Predicate)
	 */
	public MultiCastDispatcher(String bindaddr,int port,int bufferSize,
			Predicate<byte[]>processor,
			Predicate<Throwable> onerr) throws UnknownHostException{
		this(InetAddress.getByName(checkNotNull(Strings.emptyToNull(bindaddr),"bindaddr is null or empty")),
				port,bufferSize,processor,onerr);
	}
	/**
	 * 构造方法
	 * @param hostPort 组播或广播地址和端口号(:号分隔) 如：244.12.12.12:4331,或[244.12.12.12:4331]
	 * @param bufferSize
	 * @param processor
	 * @param onerr
	 * @throws UnknownHostException
	 * @see #MultiCastDispatcher(String, int, int, Predicate, Predicate)
	 */
	public MultiCastDispatcher(String hostPort,int bufferSize,
			Predicate<byte[]>processor,
			Predicate<Throwable> onerr) throws UnknownHostException{
		this(HostAndPort.fromString(hostPort).getHost(),
				HostAndPort.fromString(hostPort).getPort(),
				bufferSize,processor,onerr);
	}
	/**
	 * socket初始化
	 * @return 当前对象
	 * @throws IOException 创建Socket对象时出错
	 */
	public MultiCastDispatcher init() throws IOException{
		if(null == datagramSocket){
			if(NetworkUtil.isBroadcast(group)){
				datagramSocket = new DatagramSocket(port);
				datagramSocket.setBroadcast(true);
			}else{
				MulticastSocket multicastSocket = new MulticastSocket(port);
				for(NetworkInterface nic:NetworkUtil.getNICs(Filter.UP,Filter.ETH_NIC)){
					InetSocketAddress inetAddr= new InetSocketAddress(group,0);
					multicastSocket.joinGroup(inetAddr,nic);
				}
				this.datagramSocket = multicastSocket;
			}
		}
		return this;
	}
	/**
	 * 循环接收group,port指定的组播地址发送的数据并交给{@link #processor}处理
	 */
	@Override
	public void run() {
		stopListener = Boolean.FALSE;
		DatagramPacket packet = new DatagramPacket(message, message.length);
		try {
			while(!Boolean.TRUE .equals(stopListener)){
				try {
					checkArgument(datagramSocket != null,"multicastSocket is uninitizlied");
					datagramSocket.receive(packet);
					byte[] recevied = new byte[packet.getLength()];
					System.arraycopy(message, 0, recevied, 0, packet.getLength());
					if(!processor.apply(recevied)){
						break;
					}
				} catch (Exception e) {
					if(!onerr.apply(e)){
						break;
					}
				}
			}
		} finally {
			try {
				if(datagramSocket instanceof MulticastSocket){
					for(NetworkInterface nic:NetworkUtil.getNICs(Filter.UP,Filter.PHYICAL_ONLY)){
						InetSocketAddress inetAddr= new InetSocketAddress(group,0);
						((MulticastSocket) datagramSocket).leaveGroup(inetAddr,nic);
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			datagramSocket.close();
			datagramSocket = null;
			stopListener = null;
		}				
	}
	
	public boolean isRunning(){
		return Boolean.FALSE.equals(stopListener);
	}
	public MultiCastDispatcher running(){
		stopListener = Boolean.FALSE;
		return this;
	}	
	public synchronized void stop() {
		stopListener = Boolean.TRUE;
	}
	/**
	 * 返回组播地址IP
	 * @return group
	 */
	public InetAddress getGroup() {
		return group;
	}
	/**
	 * 返回侦听端口号
	 * @return port
	 */
	public int getPort() {
		return port;
	}
}
