package net.gdface.utils;

import java.lang.reflect.Method;

import com.google.common.collect.ImmutableList;

public interface ClassCommentProvider {
	ImmutableList<String> commentOfClass();
	ImmutableList<String> commentOfMethod(Method method);
	ImmutableList<String> commentOfField(String name);
}
