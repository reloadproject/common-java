package net.gdface.utils;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static net.gdface.utils.NetworkUtil.*;

import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;

public class NetworkUtilTest {
	private static final Logger logger = LoggerFactory.getLogger(NetworkUtilTest.class) ;
	@Test
	public void testGetPhysicalNICs() {
		logger.info("ipv4AddressesOfPhysicalNICs :{}",NetworkUtil.ipv4AddressesOfPhysicalNICs());
		logger.info("addressesOfPhysicalNICs(FILTER_IPV4) nic:{}",NetworkUtil.addressesOfPhysicalNICs(FILTER_IPV4));
		logger.info("ipv4AddressesOfNoVirtualNICs :{}",NetworkUtil.ipv4AddressesOfNoVirtualNICs());
		logger.info("addressesOfNoVirtualNICs(FILTER_IPV4):{}",NetworkUtil.addressesOfNoVirtualNICs(FILTER_IPV4));
	}

	@Test
	public void testGetCurrentMac(){
		try {
			byte[] mac = NetworkUtil.getCurrentMac("www.google.com:80","www.baidu.com:80","www.qq.com:80","www.aliyun.com:80");
			System.out.printf("MAC:%s\n", BinaryUtils.toHex(mac));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testInetAddress(){
		try {
			System.out.printf("%s loopback:%b\n", "127.0.0.1",InetAddress.getByName("127.0.0.1").isLoopbackAddress());
			System.out.printf("%s loopback:%b\n", "localhost",InetAddress.getByName("localhost").isLoopbackAddress());

		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		
	}
	@Test
	public void testNics() throws SocketException{
		for(NetworkInterface nic:NetworkUtil.getNICs(Filter.ETH_NIC)){
			ArrayList<InetAddress> addrs = Collections.list(nic.getInetAddresses());
			System.out.printf("%s,%s,hardward addr=%s,loopback=%b,virtual=%b up=%b,supportsMulticast=%b\n", 
					nic.getName(),addrs,BinaryUtils.toHex(nic.getHardwareAddress()),nic.isLoopback(),nic.isVirtual(),nic.isUp(),nic.supportsMulticast());
		}
	}
	@Test
	public void testFormatIPV6(){
		String hexipv6 = "fe80000000010000044044ff12335678";
		byte[] ipv6Address = BinaryUtils.hex2Bytes(hexipv6);
		System.out.printf("ipv6 %s format %s\n", hexipv6,NetworkUtil.formatIpv6(ipv6Address));
	}
}
