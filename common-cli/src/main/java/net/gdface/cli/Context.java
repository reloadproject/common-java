package net.gdface.cli;

import java.util.HashMap;
import java.util.Map;

/**
 * 上下文数据管理类
 * @author guyadong
 *
 */
public class Context {
	public static final class Builder {
		private final Map<String, Object> map = new HashMap<String, Object>();

		private Builder() {
		}

		public Builder addProperties(Map<String, ? extends Object> properties) {
			if (properties != null){
				map.putAll(properties);
			}
			return this;
		}

		public <T> Builder addProperty(String name, T property) {
			map.put(name, property);
			return this;
		}

		public Context build() {
			return new Context(map);
		}
		public <T extends Context> T build(Class<T> clazz) {
			if(null == clazz){
				throw new NullPointerException("clazz must not be null");
			}
			T instance;
			try {
				instance = clazz.newInstance();
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
			instance.setProperties(map);
			return instance;
		}
	}

	public static final Builder builder() {
		return new Builder();
	}

	private Map<String, Object> context = new HashMap<String, Object>();

	public Context() {
	}

	public Context(Map<String, Object> map) {
		context.putAll(map);
	}
	public Context(Context context) {
		this.context.putAll(context.getContext());
	}
	/**
	 * @return map
	 */
	public Map<String, Object> getContext() {
		return context;
	}
	public Integer getInt(String name) {
		try{
			return (Integer)context.get(name);
		}catch (ClassCastException e) {
			Object v = context.get(name);
			if(v instanceof Number){
				Number num = (Number)v;
				return num.intValue();
			}else if(v instanceof String){
				return Integer.valueOf((String)v);
			}else{
				throw e;
			}
		}
	}
	public Long getLong(String name) {
		try{
			return (Long)context.get(name);
		}catch (ClassCastException e) {
			Object v = context.get(name);
			if(v instanceof Number){
				Number num = (Number)v;
				return num.longValue();
			}else if(v instanceof String){
				return Long.valueOf((String)v);
			}else{
				throw e;
			}
		}
	}
	/**
	 * @param name
	 * @return the T value to which the specified key is mapped, or null if this map contains no mapping for the key
	 * @see java.util.Map#get(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	public <T> T getProperty(String name) {
		T value;
		try {
			value =  (T) context.get(name);			
		} catch (ClassCastException e) {
			Object v = context.get(name);
			if(v instanceof Number){
				value = (T)(((Number)v));
			}else{
				throw e;
			}
		}
		return value;

	}

	/**
	 * 判断是否存在指定名字的值
	 * @param name
	 * @return 如果存在{@code name}定义的值返回{@code true},否则返回{@code null}
	 * @see #getProperty(String)
	 */
	public boolean hasProperty(String name) {
		return getProperty(name) != null;
	}

	/**
	 * @param properties
	 * @see java.util.Map#putAll(java.util.Map)
	 */
	public void setProperties(Map<String, ? extends Object> properties) {
		context.putAll(properties);
	}

	/**
	 * @param name
	 * @param property
	 * @return the previous value associated with key, or null if there was no mapping for key
	 * @see java.util.Map#put(java.lang.Object, java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	public <T> T setProperty(String name, T property) {
		return (T) context.put(name, property);
	}

}
