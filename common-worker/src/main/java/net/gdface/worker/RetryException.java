/**   
* @Title: RetryException.java 
* @Package net.gdface.service.search 
* @Description: TODO 
* @author guyadong   
* @date 2015年5月12日 上午8:47:30 
* @version V1.0   
*/
package net.gdface.worker;

/**
 * 重试异常
 * @author guyadong
 *
 */
public class RetryException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8220165613796329725L;

	/**
	 * 
	 */
	public RetryException() {
	}

	/**
	 * @param message
	 */
	public RetryException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public RetryException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public RetryException(String message, Throwable cause) {
		super(message, cause);
	}

}
