package net.gdface.worker;

import java.util.concurrent.TimeUnit;

/**
 * 队列管理器接口
 * @author guyadong
 *
 */
public interface  QueueManager <T extends WorkData>{

	/**
	 * 返回当前队列中的任务总数
	 * 
	 * @return 当前队列中的任务总数
	 * @see java.util.Collection#size()
	 */
	public abstract int queueSize();

	/**
	 * 队列管理器是否结束
	 * 
	 * @return 队列管理器结束返回{@code true},否则返回{@code false}
	 */
	public abstract boolean isFinished();

	/**
	 * 将任务加入队列延时执行(无阻塞)<br>
	 * 并设置延迟时间{@code delayMills},如果{@code delayMills}<0则使用默认值,参见{@link #setDefaultIntervalMills(long)}
	 * 
	 * @param task 任务对象
	 * @param delayMills 指定当前时间算起多长时间(毫秒)后执行该任务
	 * @return true 
	 * @see java.util.concurrent.DelayQueue#add(java.util.concurrent.Delayed)
	 */
	public abstract  boolean push(T task, long delayMills);

	/**
	 * 将对象加队列，0延时<br>
	 * 如果队列满，则阻塞
	 * 
	 * @param task
	 * @throws InterruptedException 
	 * @see java.util.concurrent.DelayQueue#add(java.util.concurrent.Delayed)
	 */
	public abstract  void push(T task) throws InterruptedException;

	/**
	 * 从队列取出任务一个任务对象<br>
	 * 超时则抛出 {@link InterruptedException}
	 * 
	 * @param timeout
	 *            超时参数
	 * @param unit
	 *            超时参数{@code timeout}时间单位
	 * @return the head of this queue, or null if the specified waiting time elapses before an element is available
	 * @throws InterruptedException
	 * @see java.util.concurrent.BlockingQueue#poll(long, java.util.concurrent.TimeUnit)
	 */
	public abstract  T pop(long timeout, TimeUnit unit) throws InterruptedException;

	public abstract void setDefaultIntervalMills(long defaultIntervalMills);



}