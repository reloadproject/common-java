package net.gdface.common;

import static org.junit.Assert.*;

import java.lang.reflect.Constructor;

import org.junit.Test;

import net.gdface.utils.Assert;
import net.gdface.utils.MiscellaneousUtils;


public class ConstructorTest {

	@Test
	public void test() {
		try {
//			Constructor<?> ctor = C.class.getConstructor(B.class);
			Constructor<?> ctor = MiscellaneousUtils.getConstructor(C.class,B.class);
			System.out.println(ctor.toGenericString());
		} catch (NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}
	}



	public static class A {}
	public static class B extends A {}
	
	public static class C{
		private A a;
		public C(A a){
			this.a=a;
		}
	}
}
