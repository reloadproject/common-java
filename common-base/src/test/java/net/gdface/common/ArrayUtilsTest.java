package net.gdface.common;

import java.util.Arrays;

import org.junit.Test;

import static net.gdface.utils.ArrayUtils.cleanNull;

public class ArrayUtilsTest {

	@Test
	public void test() {
		Integer[] intarray = new Integer[]{1,2,3,4,5};
		Integer[] c1 = cleanNull(intarray);
		System.out.println(Arrays.toString(c1));
	}

}
