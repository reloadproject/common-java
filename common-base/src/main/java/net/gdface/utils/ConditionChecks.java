package net.gdface.utils;

import java.lang.reflect.InvocationTargetException;
import javax.annotation.Nullable;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import static net.gdface.utils.MiscellaneousUtils.getConstructor;

/**
 * 条件检查工具类
 * @author guyadong
 *
 */
public class ConditionChecks {

	private ConditionChecks() {
	}
	/**
	 * 执行表达式,为false时抛出 declareType 异常
	 * @param <X> 抛出异常类型
	 * @param b
	 * @param declareType 异常类型
	 * @param errorMessageTemplate a template for the exception message should the check fail. The
	 *     message is formed by replacing each {@code %s} placeholder in the template with an
	 *     argument. These are matched by position - the first {@code %s} gets {@code
	 *     errorMessageArgs[0]}, etc. Unmatched arguments will be appended to the formatted message in
	 *     square braces. Unmatched placeholders will be left as-is.
	 * @param errorMessageArgs the arguments to be substituted into the message template. Arguments
	 *     are converted to strings using {@link String#valueOf(Object)}.
	 * @throws X
	 */
	public static <X extends Throwable> void checkTrue(
			boolean b, 
			Class<X> declareType, 
			@Nullable String errorMessageTemplate, @Nullable Object... errorMessageArgs) throws X {
		if (!b) {
			try {
				throw checkNotNull(declareType, "declareType is null").getConstructor(String.class).newInstance(format(errorMessageTemplate, errorMessageArgs));
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException | NoSuchMethodException | SecurityException e) {
				e.printStackTrace();
			}
		}
	}
	/**
	 * 执行表达式,为false时抛出 declareType 异常
	 *
	 * <p>See {@link #checkTrue(boolean, Class, String, Object...)} for details.
	 * @throws X 
	 */
	public static <X extends Throwable> void checkTrue(
			boolean b, 
			Class<X> declareType, 
			@Nullable String errorMessageTemplate, @Nullable Object p1) throws X {
		if (!b) {
			try {
				throw checkNotNull(declareType, "declareType is null").getConstructor(String.class).newInstance(format(errorMessageTemplate, p1));
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException | NoSuchMethodException | SecurityException e) {
				e.printStackTrace();
			}	
		}
	}
	/**
	 * 执行表达式,为false时抛出 declareType 异常
	 * @param <X> 抛出异常类型
	 * @param b
	 * @param declareType 异常类型,要求有两个参数的构造方法(ctorArg,String)或(String,ctorArg)
	 * @param ctorArg 异常类型构造方法的第一或第二个参数,另一个参数类型为{@link String}
	 * @param errorMessageTemplate a template for the exception message should the check fail. The
	 *     message is formed by replacing each {@code %s} placeholder in the template with an
	 *     argument. These are matched by position - the first {@code %s} gets {@code
	 *     errorMessageArgs[0]}, etc. Unmatched arguments will be appended to the formatted message in
	 *     square braces. Unmatched placeholders will be left as-is.
	 * @param errorMessageArgs the arguments to be substituted into the message template. Arguments
	 *     are converted to strings using {@link String#valueOf(Object)}.
	 * @throws X
	 */
	public static <X extends Throwable> void checkTrue(
			boolean b, 
			Class<X> declareType, 
			Object ctorArg,
			@Nullable String errorMessageTemplate, @Nullable Object... errorMessageArgs) throws X {
		if (!b) {
			try {
				checkNotNull(declareType, "declareType is null");
				checkNotNull(ctorArg, "ctorArg is null");
				try {
					throw getConstructor(declareType, ctorArg.getClass(),String.class).newInstance(ctorArg,format(errorMessageTemplate, errorMessageArgs));
				} catch (NoSuchMethodException e) {
					throw getConstructor(declareType, String.class,ctorArg.getClass()).newInstance(format(errorMessageTemplate, errorMessageArgs),ctorArg);
				}
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException | NoSuchMethodException |SecurityException e) {
				e.printStackTrace();
			}
		}
	}
	/**
	 * 执行表达式,为false时抛出 declareType 异常
	 *
	 * <p>See {@link #checkTrue(boolean, Class,Object, String, Object...)} for details.
	 * @throws X 
	 */
	public static <X extends Throwable> void checkTrue(
			boolean b, 
			Class<X> declareType, 
			Object ctorArg,
			@Nullable String errorMessageTemplate, @Nullable Object p1) throws X {
		if (!b) {
			try {
				checkNotNull(declareType, "declareType is null");
				checkNotNull(ctorArg, "ctorArg is null");
				try{
					throw getConstructor(declareType, ctorArg.getClass(),String.class).newInstance(ctorArg,format(errorMessageTemplate, p1));
				} catch (NoSuchMethodException e) {
					throw getConstructor(declareType, String.class,ctorArg.getClass()).newInstance(format(errorMessageTemplate, p1),ctorArg);
				}
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException | NoSuchMethodException | SecurityException e) {
				e.printStackTrace();
			}	
		}
	}
	/**
	 * 执行表达式,为false时抛出 declareType 异常
	 * @param <X> 抛出异常类型
	 * @param b
	 * @param declareType 异常类型
	 * @param ctorArg 异常类型构造方法的参数
	 * @throws X
	 */
	public static <X extends Throwable> void checkTrue(
			boolean b, 
			Class<X> declareType, 
			Object ctorArg
			) throws X {
		if (!b) {
			try {
				checkNotNull(declareType, "declareType is null");
				checkNotNull(ctorArg, "ctorArg is null");
				throw getConstructor(declareType, ctorArg.getClass()).newInstance(ctorArg);
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException | NoSuchMethodException | SecurityException e) {
				e.printStackTrace();
			}	
		}
	}
	/**
	 * reference为{@code null}时抛出 declareType 异常
	 * @param <T> 对象类型
	 * @param <X> 抛出异常类型
	 * @param reference
	 * @param declareType 异常类型
	 * @param errorMessageTemplate a template for the exception message should the check fail. The
	 *     message is formed by replacing each {@code %s} placeholder in the template with an
	 *     argument. These are matched by position - the first {@code %s} gets {@code
	 *     errorMessageArgs[0]}, etc. Unmatched arguments will be appended to the formatted message in
	 *     square braces. Unmatched placeholders will be left as-is.
	 * @param errorMessageArgs the arguments to be substituted into the message template. Arguments
	 *     are converted to strings using {@link String#valueOf(Object)}.
	 * @return  always reference
	 * @throws X
	 */
	@CanIgnoreReturnValue
	public static <T,X extends Throwable> T checkNotNull(
			T reference, 
			Class<X> declareType, 
			@Nullable String errorMessageTemplate, @Nullable Object... errorMessageArgs) throws X {
		if (null==reference) {
			try {
				throw checkNotNull(declareType,"declareType is null").getConstructor(String.class).newInstance(format(errorMessageTemplate, errorMessageArgs));
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException | NoSuchMethodException | SecurityException e) {
				e.printStackTrace();
			}
		}
		return reference;
	}
	/**
	 * reference为{@code null}时抛出 declareType 异常
	 *
	 * <p>See{@link #checkNotNull(Object, Class, String, Object[])} for details.
	 * @return always reference
	 * @throws X 
	 */
	@CanIgnoreReturnValue
	public static <T,X extends Throwable> T checkNotNull(
			T reference,
			Class<X> declareType, 
			@Nullable String errorMessageTemplate, @Nullable Object p1) throws X {
		if (null==reference) {
			try {
				throw checkNotNull(declareType,"declareType is null").getConstructor(String.class).newInstance(format(errorMessageTemplate, p1));
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException | NoSuchMethodException | SecurityException e) {
				e.printStackTrace();
			}	
		}
		return reference;
	}
	/**
	 * reference为{@code null}时抛出 declareType 异常
	 * @param <T> 对象类型
	 * @param <X> 抛出异常类型
	 * @param reference
	 * @param declareType 异常类型,要求有两个参数的构造方法(ctorArg,String)或(String,ctorArg)
	 * @param ctorArg 异常类型构造方法的第一或第二个参数,另一个参数类型为{@link String}
	 * @param errorMessageTemplate a template for the exception message should the check fail. The
	 *     message is formed by replacing each {@code %s} placeholder in the template with an
	 *     argument. These are matched by position - the first {@code %s} gets {@code
	 *     errorMessageArgs[0]}, etc. Unmatched arguments will be appended to the formatted message in
	 *     square braces. Unmatched placeholders will be left as-is.
	 * @param errorMessageArgs the arguments to be substituted into the message template. Arguments
	 *     are converted to strings using {@link String#valueOf(Object)}.
	 * @return  T instance
	 * @throws X
	 */
	@CanIgnoreReturnValue
	public static <T,X extends Throwable> T checkNotNull(
			T reference, 
			Class<X> declareType, 
			Object ctorArg,
			@Nullable String errorMessageTemplate, @Nullable Object... errorMessageArgs) throws X {
		if (null==reference) {
			try {
				checkNotNull(declareType, "declareType is null");
				checkNotNull(ctorArg, "ctorArg is null");
				try{
					throw getConstructor(declareType, ctorArg.getClass(),String.class).newInstance(ctorArg,format(errorMessageTemplate, errorMessageArgs));
				}catch (NoSuchMethodException e) {
					throw getConstructor(declareType,String.class, ctorArg.getClass()).newInstance(format(errorMessageTemplate, errorMessageArgs),ctorArg);
				}
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException | NoSuchMethodException | SecurityException e) {
				e.printStackTrace();
			}
		}
		return reference;
	}
	
	/**
	 * reference为{@code null}时抛出 declareType 异常
	 *
	 * <p>See{@link #checkNotNull(Object, Class, Object, String, Object[])} for details.
	 * @return always reference
	 * @throws X 
	 */
	@CanIgnoreReturnValue
	public static <T,X extends Throwable> T checkNotNull(
			T reference,
			Class<X> declareType, 
			Object ctorArg,
			@Nullable String errorMessageTemplate, @Nullable Object p1) throws X {
		if (null==reference) {
			try {
				checkNotNull(declareType, "declareType is null");
				checkNotNull(ctorArg, "ctorArg is null");
				try{
					throw getConstructor(declareType, ctorArg.getClass(),String.class).newInstance(ctorArg,format(errorMessageTemplate, p1));
				}catch (NoSuchMethodException e) {
					throw getConstructor(declareType,String.class, ctorArg.getClass()).newInstance(format(errorMessageTemplate, p1),ctorArg);
				}
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException | NoSuchMethodException | SecurityException e) {
				e.printStackTrace();
			}	
		}
		return reference;
	}
	/**
	 * reference为{@code null}时抛出 declareType 异常
	 * @param <X> 抛出异常类型
	 * @param reference
	 * @param declareType 异常类型
	 * @param ctorArg 异常类型构造方法的参数
	 * @return always reference
	 * @throws X
	 */
	@CanIgnoreReturnValue
	public static <T,X extends Throwable> T checkNotNull(
			T reference,
			Class<X> declareType, 
			Object ctorArg) throws X {
		if (null==reference) {
			try {
				checkNotNull(declareType, "declareType is null");
				checkNotNull(ctorArg, "ctorArg is null");
				throw getConstructor(declareType, ctorArg.getClass()).newInstance(ctorArg);
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException | NoSuchMethodException | SecurityException e) {
				e.printStackTrace();
			}	
		}
		return reference;
	}
	/**
	 * Ensures the truth of an expression involving the state of the calling instance, but not
	 * involving any parameters to the calling method.
	 *
	 * @param expression a boolean expression
	 * @throws IllegalStateException if {@code expression} is false
	 */
	public static void checkState(boolean expression) {
		if (!expression) {
			throw new IllegalStateException();
		}
	}
	/**
	 * Ensures the truth of an expression involving the state of the calling instance, but not
	 * involving any parameters to the calling method.
	 *
	 * @param expression a boolean expression
	 * @param errorMessageTemplate a template for the exception message should the check fail. The
	 *     message is formed by replacing each {@code %s} placeholder in the template with an
	 *     argument. These are matched by position - the first {@code %s} gets {@code
	 *     errorMessageArgs[0]}, etc. Unmatched arguments will be appended to the formatted message in
	 *     square braces. Unmatched placeholders will be left as-is.
	 * @param errorMessageArgs the arguments to be substituted into the message template. Arguments
	 *     are converted to strings using {@link String#valueOf(Object)}.
	 * @throws IllegalStateException if {@code expression} is false
	 * @throws NullPointerException if the check fails and either {@code errorMessageTemplate} or
	 *     {@code errorMessageArgs} is null (don't let this happen)
	 */
	public static void checkState(
			boolean expression,
			@Nullable String errorMessageTemplate,
			@Nullable Object... errorMessageArgs) {
		if (!expression) {
			throw new IllegalStateException(format(errorMessageTemplate, errorMessageArgs));
		}
	}

	/**
	 * Ensures the truth of an expression involving one or more parameters to the calling method.
	 *
	 * @param expression a boolean expression
	 * @throws IllegalArgumentException if {@code expression} is false
	 */
	public static void checkArgument(boolean expression) {
		if (!expression) {
			throw new IllegalArgumentException();
		}
	}
	/**
	 * Ensures the truth of an expression involving one or more parameters to the calling method.
	 *
	 * @param expression a boolean expression
	 * @param errorMessageTemplate a template for the exception message should the check fail. The
	 *     message is formed by replacing each {@code %s} placeholder in the template with an
	 *     argument. These are matched by position - the first {@code %s} gets {@code
	 *     errorMessageArgs[0]}, etc. Unmatched arguments will be appended to the formatted message in
	 *     square braces. Unmatched placeholders will be left as-is.
	 * @param errorMessageArgs the arguments to be substituted into the message template. Arguments
	 *     are converted to strings using {@link String#valueOf(Object)}.
	 * @throws IllegalArgumentException if {@code expression} is false
	 * @throws NullPointerException if the check fails and either {@code errorMessageTemplate} or
	 *     {@code errorMessageArgs} is null (don't let this happen)
	 */
	public static void checkArgument(
			boolean expression,
			@Nullable String errorMessageTemplate,
			@Nullable Object... errorMessageArgs) {
		if (!expression) {
			throw new IllegalArgumentException(format(errorMessageTemplate, errorMessageArgs));
		}
	}
	/**
	 * Ensures that an object reference passed as a parameter to the calling method is not null.
	 *
	 * @param reference an object reference
	 * @return the non-null reference that was validated
	 * @throws NullPointerException if {@code reference} is null
	 */
	@CanIgnoreReturnValue
	public static <T> T checkNotNull(T reference) {
		if (reference == null) {
			throw new NullPointerException();
		}
		return reference;
	}
	/**
	 * Ensures that an object reference passed as a parameter to the calling method is not null.
	 *
	 * @param reference an object reference
	 * @param errorMessageTemplate a template for the exception message should the check fail. The
	 *     message is formed by replacing each {@code %s} placeholder in the template with an
	 *     argument. These are matched by position - the first {@code %s} gets {@code
	 *     errorMessageArgs[0]}, etc. Unmatched arguments will be appended to the formatted message in
	 *     square braces. Unmatched placeholders will be left as-is.
	 * @param errorMessageArgs the arguments to be substituted into the message template. Arguments
	 *     are converted to strings using {@link String#valueOf(Object)}.
	 * @return the non-null reference that was validated
	 * @throws NullPointerException if {@code reference} is null
	 */
	@CanIgnoreReturnValue
	public static <T> T checkNotNull(
			T reference, @Nullable String errorMessageTemplate, @Nullable Object... errorMessageArgs) {
		if (reference == null) {
			// If either of these parameters is null, the right thing happens anyway
			throw new NullPointerException(format(errorMessageTemplate, errorMessageArgs));
		}
		return reference;
	}
	/**
	 * Substitutes each {@code %s} in {@code template} with an argument. These are matched by
	 * position: the first {@code %s} gets {@code args[0]}, etc. If there are more arguments than
	 * placeholders, the unmatched arguments will be appended to the end of the formatted message in
	 * square braces.
	 *
	 * @param template a non-null string containing 0 or more {@code %s} placeholders.
	 * @param args the arguments to be substituted into the message template. Arguments are converted
	 *     to strings using {@link String#valueOf(Object)}. Arguments can be null.
	 */
	// Note that this is somewhat-improperly used from Verify.java as well.
	public static String format(String template, @Nullable Object... args) {
		template = String.valueOf(template); // null -> "null"

		// start substituting the arguments into the '%s' placeholders
		StringBuilder builder = new StringBuilder(template.length() + 16 * args.length);
		int templateStart = 0;
		int i = 0;
		while (i < args.length) {
			int placeholderStart = template.indexOf("%s", templateStart);
			if (placeholderStart == -1) {
				break;
			}
			builder.append(template, templateStart, placeholderStart);
			builder.append(args[i++]);
			templateStart = placeholderStart + 2;
		}
		builder.append(template, templateStart, template.length());

		// if we run out of placeholders, append the extra args in square braces
		if (i < args.length) {
			builder.append(" [");
			builder.append(args[i++]);
			while (i < args.length) {
				builder.append(", ");
				builder.append(args[i++]);
			}
			builder.append(']');
		}

		return builder.toString();
	}
}
