package net.gdface.utils;

import java.lang.reflect.Array;
import java.util.Arrays;

public class ArrayUtils {

	private ArrayUtils() {}
	/**
	 * 清除array中为{@code null}的元素
	 * @param array 输入的数组
	 * @return 如果 array为{@code null}或长度为0或array中没有{@code null}元素则返回array,否则返回新的不含{@code null}的数组
	 */
	@SuppressWarnings("unchecked")
	public static final <T> T[] cleanNull(T[] array){
		if(null == array || array.length == 0){
			return array;
		}		
		T[] copy = (T[]) Array.newInstance(array.getClass().getComponentType(), array.length);
		int c = 0;
		for(int i=0; i < array.length; ++i){
			if(null != array[i]){
				copy[c++] = array[i];
			}
		}
		return c == array.length ? array : Arrays.copyOf(copy, c);
	}
    /**
     * 返回第一个为{@code null}元素的索引,没找到返回-1,如果输入为{@code null}则返回-2
     * @param objects
     */
    public static int indexOfFirstNull(Object...objects) {
		if(objects != null){
			for(int i=0;i< objects.length;++i){
				if(null == objects[i]){
					return i;
				}
			}
			return -1;
		}
		return -2;
	}
	/**
	 * @param objects 
	 * @return true if any one of object is null or objects is null 
	 */
	public static boolean hasNull(Object...objects) {
		return indexOfFirstNull(objects) != -1;
	}
}
