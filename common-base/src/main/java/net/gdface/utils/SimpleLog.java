package net.gdface.utils;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 简单日志输出工具类
 * @author guyadong
 *
 */
public class SimpleLog {
	/** 占位符 */
	private static final String DELIM_STR = "(\\{\\}|%s)";
	private static final Object[] EMPTY_ARGS = new Object[0];
	/**
	 * 返回格式化的日志信息字符串<br>
	 * example:
	 * <pre>
	 * logString("name : {},age:{}","tom",23);
	 * </pre>
	 * @param format 格式字符串,采用"{}"或"%s"为占位符
	 * @param args 填充占位符的参数列表,如果数量小于占位符个数则多出的占位符填充"null"
	 */
	public static String logString(String format, Object ... args){
		if(null == format){
			return "";
		}
		if(null == args){
			args = EMPTY_ARGS;
		}
		StringBuilder buffer = new StringBuilder(format.length() + 64);
		int beginIndex = 0,count = 0;
		Pattern pattern = Pattern.compile(DELIM_STR);
		Matcher matcher = pattern.matcher(format);
		while(matcher.find()){
			buffer.append(format.substring(beginIndex,matcher.start()));
			try{
				buffer.append(args[count++]);
			}catch(IndexOutOfBoundsException e){
				// 数组越界时对应占位符填null
				buffer.append("null");
			}
			beginIndex = matcher.end();
		}
		buffer.append(format.substring(beginIndex,format.length()));
		return buffer.toString();
	}
	private static void log(PrintStream printStream,int level, String format, Object ... args){
		if(null == printStream){
			return;
		}
		Thread currentThread = Thread.currentThread();
		StackTraceElement stackTrace = currentThread.getStackTrace()[level];
		printStream.printf("[%s] (%s:%d) %s\n",
				currentThread.getName(),
				stackTrace.getFileName(),
				stackTrace.getLineNumber(),
				logString(format,args));
	}
	/**
	 * 向{@code printStream}输出日志信息<br>
	 * example:
	 * <pre>
	 * log("name : {},age:{}","tom",23);
	 * </pre>
	 * @param printStream
	 * @param format 格式字符串,采用"{}"或"%s"为占位符,占位符个数要与{@code args}数组长度匹配
	 * @param args 填充占位符的参数列表,如果数量小于占位符个数则多出的占位符填充"null"
	 */
	public static void log(PrintStream printStream,String format, Object ... args){
		log(printStream,3,format,args);	
	}
	/**
	 * @param output 是否输出
	 * @param printStream
	 * @param format 格式字符串
	 * @param args 填充占位符的参数列表
	 * @see #log(PrintStream, String, Object...)
	 */
	public static void log(boolean output,PrintStream printStream,String format, Object ... args){
		if(output){
			log(printStream,3,format,args);
		}
	}
	private static final String formatByCount(int c){
		StringBuffer buffer = new StringBuffer();
		for(int i=0;i<c;++i){
			if(i>0){
				buffer.append(",");
			}
			buffer.append("{}");
		}
		return buffer.toString();
	}
	/**
	 * 向控制台输出日志信息<br>
	 * @param arg
	 * @see #log(PrintStream, String, Object...)
	 */
	public static void logObjects(Object arg){
		log(System.out,3,formatByCount(1),new Object[]{arg});
	}
	/**
	 * 向控制台输出日志信息<br>
	 * @param arg
	 * @param args
	 * @see #log(PrintStream, String, Object...)
	 */
	public static void logObjects(Object arg,Object ... args){
		Object[] array = new Object[args.length+1];
		array[0] = arg;
		System.arraycopy(args, 0, array, 1, args.length);
		log(System.out,3,formatByCount(args.length),array);
	}
	/**
	 * 向控制台输出日志信息<br>
	 * @param format 格式字符串,采用"{}"或"%s"为占位符
	 * @param args
	 * @see #log(PrintStream, String, Object...)
	 */
	public static void log(String format, Object ... args){
		log(System.out,3,format,args);
	}
	/**
	 * 向控制台输出日志信息<br>
	 * @param output 是否输出
	 * @param format 格式字符串,采用"{}"或"%s"为占位符
	 * @param args
	 * @see #log(PrintStream, String, Object...)
	 */
	public static void log(boolean output,String format, Object ... args){
		if(output){
			log(System.out,3,format,args);
		}
	}
	
	public static final String stackTraceOf(Throwable e){
		if(e == null){			
			e = new NullPointerException("e is null");
		}
		
		StringWriter writer = new StringWriter();
		PrintWriter pw = new PrintWriter(writer);
		e.printStackTrace(pw);
		return writer.toString();
	}
	public static void log(String msg,Throwable e){
		log(System.out,3,"{}\n{}",msg,stackTraceOf(e));
	}
	public static void log(Throwable e){
		log(System.out,3,"{}",stackTraceOf(e));
	}
}
