# apache bytecode

copy from `org.apache.axis2.description.java2wsdl.bytecode` in [axis2-kernel-1.6.2-sources.jar](http://central.maven.org/maven2/org/apache/axis2/axis2-kernel/1.6.2/axis2-kernel-1.6.2-sources.jar)

更新 `org.apache.bytecode.ClassReader.java` from axis2-kernel 1.8.0-SNAPSTHOT(1.7.9之后的版本)[org.apache.axis2.description.java2wsdl.bytecode.ClassReader](https://svn.apache.org/repos/asf/axis/axis2/java/core/trunk/modules/kernel/src/org/apache/axis2/description/java2wsdl/bytecode),
以支持java 高版本(8 9 10 11)生成的class
