package net.gdface.image;

import java.awt.image.BufferedImage;
import java.io.File;

import net.gdface.utils.Assert;

public class LazyImageFactoryByBufferedImage implements LazyImageFactory {

	public LazyImageFactoryByBufferedImage() {
	}

	@Override
	public BaseLazyImage create(byte[] imgBytes) throws NotImageException, UnsupportedFormatException {
		return LazyImage.create(imgBytes);
	}

	@Override
	public BaseLazyImage create(File file, String md5) throws NotImageException, UnsupportedFormatException {
		return LazyImage.create(file, md5);
	}

	@Override
	public <T> BaseLazyImage create(T src) throws NotImageException, UnsupportedFormatException {
		if(src instanceof BufferedImage){
			return LazyImage.create((BufferedImage)src);
		}
		return LazyImage.create(src);
	}
	@Override
	public <T> BaseLazyImage createByImageObject(T imgObj){
		Assert.notNull(imgObj, "imageObj");
		if(imgObj instanceof BufferedImage){
			return LazyImage.create((BufferedImage)imgObj);
		}
		throw new IllegalArgumentException(
				String.format("UNSUPPORTED image type: %s, java.awt.image.BufferedImage required",
						imgObj.getClass().getName()));
	}
}
