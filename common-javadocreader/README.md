# javadocreader

## 介绍
通过javadoc从源码中读取注释(comments)

## 依赖库导入

Maven 

	<dependency>
	    <groupId>com.gitee.l0km</groupId>
	    <artifactId>javadocreader</artifactId>
	    <version>1.0.0</version>
	</dependency>

Gradle

	compile group: 'com.gitee.l0km', name: 'javadocreader', version: '1.0.0'


## 使用示例

参见:[gu.doc.RootDocTest.java](src/test/java/gu/doc/RootDocTest.java)

