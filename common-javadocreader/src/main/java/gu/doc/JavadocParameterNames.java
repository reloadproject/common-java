package gu.doc;

import java.lang.reflect.Member;
import com.sun.javadoc.ClassDoc;
import net.gdface.utils.BaseParameterNames;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * 基于JavaDoc解析类的源码实现获取构造函数或方法的参数名<br>
 * @author guyadong
 *
 */
public class JavadocParameterNames extends BaseParameterNames {
	private final ExtClassDoc extClassDoc;

	public JavadocParameterNames(Class<?> clazz) {
		super(clazz);
		this.extClassDoc = JavadocReader.read(clazz);
	}
	public JavadocParameterNames(ClassDoc classDoc) {
		super(toClass(classDoc));
		this.extClassDoc = new ExtClassDoc(classDoc);
	}
	
	public JavadocParameterNames(ExtClassDoc extClassDoc) {
		super(toClass(extClassDoc));
		this.extClassDoc = extClassDoc;
	}
	private static Class<?> toClass(ClassDoc classDoc){
		try {
			return Class.forName(checkNotNull(classDoc,"classDoc is null").qualifiedName());
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}
	private static Class<?> toClass(ExtClassDoc extClassDoc){
		return toClass(checkNotNull(extClassDoc,"extClassDoc is null").getClassDoc());
	}
	@Override
	protected String[] doGetParameterNames(Member member) {
		if(extClassDoc != null && clazz != null){
			ClassDoc classDoc = extClassDoc.getClassDoc();
			if(classDoc != null){
				return extClassDoc.getParamerNames(member);				
			}
		}
		return null;
	}

}
