package com.facebook.swift.codec.metadata;

import java.lang.reflect.Method;

import com.google.common.collect.ImmutableList;

/**
 * @deprecated replaced by common-base2 net.gdface.utils.ClassCommentProvider
 *
 */
public interface ClassCommentProvider {
	ImmutableList<String> commentOfClass();
	ImmutableList<String> commentOfMethod(Method method);
	ImmutableList<String> commentOfField(String name);
}
