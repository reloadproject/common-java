package com.facebook.swift.codec.metadata;

import com.facebook.swift.codec.ThriftEnumValue;

/**
 * 代理服务thrift的ERPC服务调用返回状态<br>
 * eRPC中枚举变量值不能为-1,所以这里错误代码定义从-2开始
 * @author guyadong
 *
 */
public enum ErpcProxyReturnCode {
	/** Argument is an invalid value */ INVALID_ARGUMENT(-9),
	/** thrift Server is stopped */ SERVER_IS_DOWN(-8),
	/** thrift exception */ THRIFT_ERROR(-7),
	/** out of memory */ MEMORY_ERROR(-6),
	/** runtime error by proxy self */ RUNTIME_ERROR(-5),
	/** Operated timed out */ TIMEOUT(-4),
	/** Generic failure */ FAIL(-3),
	/** empty message returned*/ EMPTY_REPLY(-2),
	/** No error occurred */ SUCCESS(0);	
	private Integer value;
	private ErpcProxyReturnCode(){}
	private ErpcProxyReturnCode(int value){
		this.value = value;
	}
	@ThriftEnumValue
	public Integer getConst(){
		return value;
	}
	
}
