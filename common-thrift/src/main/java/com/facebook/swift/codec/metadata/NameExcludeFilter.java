package com.facebook.swift.codec.metadata;

import java.util.Arrays;
import java.util.Collections;
import java.util.Set;

import com.google.common.base.MoreObjects;
import com.google.common.base.Predicate;
import com.google.common.collect.Sets;

public class NameExcludeFilter implements Predicate<String> {
	private final Set<String> excludeMethods;

	public NameExcludeFilter(Set<String> excludeNames) {
		super();
		this.excludeMethods = MoreObjects.firstNonNull(excludeNames,Collections.<String>emptySet());
	}
	public NameExcludeFilter(Iterable<String> excludeNames) {
		this(Sets.newLinkedHashSet(excludeNames));
	}
	public NameExcludeFilter(String ...excludeNames) {
		this(Arrays.asList(excludeNames));
	}
	@Override
	public boolean apply(String input) {
		if(excludeMethods.contains(input)){
			return false;
		}
		for(String pattern:excludeMethods){
			if(PatternFilter.filter(pattern,input)){
				return false;
			}
		}
		return true;
	}
	/**
	 * @return excludeMethods
	 */
	public Set<String> getExcludeMethods() {
		return excludeMethods;
	}

	
}
