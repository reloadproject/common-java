package com.facebook.swift.service;

import org.apache.thrift.protocol.TProtocolFactory;
import org.jboss.netty.channel.ChannelHandler;
import org.jboss.netty.handler.codec.http.HttpServerCodec;

import com.facebook.nifty.codec.ThriftFrameCodecFactory;

public class ThriftHttpCodecFactory implements ThriftFrameCodecFactory {

	public ThriftHttpCodecFactory() {
		super();
	}

	@Override
	public ChannelHandler create(int maxFrameSize, TProtocolFactory defaultProtocolFactory) {
		return new HttpServerCodec();
	}

}
